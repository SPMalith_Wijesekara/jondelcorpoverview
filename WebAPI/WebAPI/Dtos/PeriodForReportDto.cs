using System.ComponentModel.DataAnnotations;

namespace Dtos
{
    public class PeriodForReportDto
    {
        
        public int StartYear { get; set; }      
       
        public int StartMonth { get; set; }
      
        public int EndYear { get; set; }      
       
        public int EndMonth { get; set; }
    }
}