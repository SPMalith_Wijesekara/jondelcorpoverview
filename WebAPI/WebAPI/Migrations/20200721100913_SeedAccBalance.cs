﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebAPI.Migrations
{
    public partial class SeedAccBalance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[AccountBalance] ([Id], [Year], [Month], [RAndD], [Canteen], [CeoCar], [Marketing], [ParkingFines]) VALUES (N'8E52D3CF-EEB8-465F-F5AE-08D82D3468AE', 2018, 5, 25000, 27000, -12000, -55000, -4400.4)
                INSERT INTO [dbo].[AccountBalance] ([Id], [Year], [Month], [RAndD], [Canteen], [CeoCar], [Marketing], [ParkingFines]) VALUES (N'294FC453-2444-4998-F5AF-08D82D3468AE', 2018, 7, 15000, 30000, -12000, -30000, -4400.4)
                INSERT INTO [dbo].[AccountBalance] ([Id], [Year], [Month], [RAndD], [Canteen], [CeoCar], [Marketing], [ParkingFines]) VALUES (N'2CCAEC4E-89F7-4835-F5B0-08D82D3468AE', 2018, 8, 15000, 52000, -12000, -15000, -4400.4)
                INSERT INTO [dbo].[AccountBalance] ([Id], [Year], [Month], [RAndD], [Canteen], [CeoCar], [Marketing], [ParkingFines]) VALUES (N'54AF6A8F-C59F-4690-F5B1-08D82D3468AE', 2018, 9, 30000, 59000, -12000, -5000, -4400.4)
                INSERT INTO [dbo].[AccountBalance] ([Id], [Year], [Month], [RAndD], [Canteen], [CeoCar], [Marketing], [ParkingFines]) VALUES (N'21BE21BE-63F6-4833-4EC4-08D82D3517EC', 2018, 11, 45000, 65000, -12000, 15000, -5500.9)
                INSERT INTO [dbo].[AccountBalance] ([Id], [Year], [Month], [RAndD], [Canteen], [CeoCar], [Marketing], [ParkingFines]) VALUES (N'00507D4E-6261-47BD-4EC5-08D82D3517EC', 2018, 12, 90000, 100000, -12000, -25000, -6400.9)
                INSERT INTO [dbo].[AccountBalance] ([Id], [Year], [Month], [RAndD], [Canteen], [CeoCar], [Marketing], [ParkingFines]) VALUES (N'B1B28621-3CA7-4295-4EC6-08D82D3517EC', 2019, 1, 105000.5, 95000, -10000, -5000, -20000)
                INSERT INTO [dbo].[AccountBalance] ([Id], [Year], [Month], [RAndD], [Canteen], [CeoCar], [Marketing], [ParkingFines]) VALUES (N'1F5DC0F4-426D-4BB5-E73D-08D82D389516', 2019, 2, 150000, 110000, -5000, -5000, -20000)
                INSERT INTO [dbo].[AccountBalance] ([Id], [Year], [Month], [RAndD], [Canteen], [CeoCar], [Marketing], [ParkingFines]) VALUES (N'0DA82707-92F5-494A-E73E-08D82D389516', 2019, 3, 125000, 150000, 9000, -5000.5, -30000)
                INSERT INTO [dbo].[AccountBalance] ([Id], [Year], [Month], [RAndD], [Canteen], [CeoCar], [Marketing], [ParkingFines]) VALUES (N'9E3BF522-6D16-43C5-E73F-08D82D389516', 2019, 4, 160000, 120000, 45660, -5000.5, -12600)
                INSERT INTO [dbo].[AccountBalance] ([Id], [Year], [Month], [RAndD], [Canteen], [CeoCar], [Marketing], [ParkingFines]) VALUES (N'1ED85CEB-7388-452E-E740-08D82D389516', 2019, 5, 250000, 160000, 120060.5, -12440.5, -12000)
                INSERT INTO [dbo].[AccountBalance] ([Id], [Year], [Month], [RAndD], [Canteen], [CeoCar], [Marketing], [ParkingFines]) VALUES (N'4467FD8F-DBF7-4FF3-1903-08D82D3A0914', 2019, 6, 120000, 140000, 122000, -24000, -36000)
                INSERT INTO [dbo].[AccountBalance] ([Id], [Year], [Month], [RAndD], [Canteen], [CeoCar], [Marketing], [ParkingFines]) VALUES (N'9818F666-B28D-43D5-1904-08D82D3A0914', 2019, 7, 105000.5, 90000, 100000, -15000, -320000)
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AccountBalance");
        }
    }
}
