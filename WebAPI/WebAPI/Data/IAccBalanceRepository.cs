using System.Collections.Generic;
using System.Threading.Tasks;
using Dtos;
using WebAPI.Models;

namespace WebAPI.Data
{
    public interface IAccBalanceRepository
    {
        void Add<T>(T entity) where T: class;
        Task<bool> SaveAll();
        Task<List<int>> GetYearList();
         Task<List<int>> GetMonthList(int Year);
        Task<AccountBalance> GetAccBalance(int Year, int Month);        
        Task<AccountBalance> AddAccBalance(AccountBalance balanceSheet);
        Task<List<AccountBalance>> GetReport(PeriodForReportDto periodForReportDto);
    }
}
