using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebAPI.Models
{
    public class AccountBalance
    {
        public Guid Id { get; set; }        
        public int Year { get; set; }       
        public int Month { get; set; }
        public double RAndD { get; set; }
        public double Canteen { get; set; }
        public double CeoCar { get; set; }
        public double Marketing { get; set; }
        public double ParkingFines { get; set; }        
        
    }
}