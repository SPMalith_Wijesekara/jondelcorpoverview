/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { AccBalanceComponent } from './acc-balance.component';
import { AccBalanceService } from '../_services/acc-balance.service';
import { AuthService } from '../_services/auth.service';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AlertifyService } from '../_services/alertify.service';

describe('AccBalanceComponent', () => {
  let component: AccBalanceComponent;
  let fixture: ComponentFixture<AccBalanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AccBalanceComponent],
      imports: [HttpClientTestingModule, RouterTestingModule, FormsModule],
      providers: [AccBalanceService, HttpClientTestingModule, FormsModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccBalanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
