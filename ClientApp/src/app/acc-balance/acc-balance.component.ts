import { Balance } from './../_models/Balance';
import { Component, OnInit } from '@angular/core';
import { AlertifyService } from '../_services/alertify.service';
import { AccBalanceService } from '../_services/acc-balance.service';
import { MonthMap } from '../_models/MonthMap';

@Component({
  selector: 'app-acc-balance',
  templateUrl: './acc-balance.component.html'
})
export class AccBalanceComponent implements OnInit {
  accountModel: any = {};
  balance: Balance = null;
  yearList: any[] = [];
  monthtList: any[] = [];

  constructor(
    private alertifyService: AlertifyService,
    private balanceService: AccBalanceService
  ) {}

  ngOnInit(): void {
    this.getYearList();
  }

  searchBalance(): void {
    if (this.accountModel.year === null || this.accountModel.month === null) {
      this.alertifyService.warning('Please select both year and month');
      return;
    }

    this.balanceService.getFilteredBalance(this.accountModel).subscribe(
      (data: Balance) => {
        if (data) {
          this.balance = data;
        }
      },
      (error) => {
        this.alertifyService.error(error.error);
      }
    );
  }

  getYearList(): void {
    this.balanceService.getYearList().subscribe(
      (data: any) => {
        if (data) {
          this.yearList = data;
        }
      },
      (error) => {
        this.alertifyService.error(error.statusText);
      }
    );
  }

  getMonthList(year): void {
    this.balanceService.getMonthList(year).subscribe(
      (data: any) => {
        if (data) {
          this.monthtList = this.balanceService.mapMonthId(data);
          this.accountModel.month = null;
        }
      },
      (error) => {
        this.alertifyService.error(error.error);
      }
    );
  }
}
