import { Component, OnInit } from '@angular/core';
import { AlertifyService } from './../_services/alertify.service';
import { AuthService } from './../_services/auth.service';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [`
    .form-group{
    margin-top: 10%;
   }
   .card{
       margin-top: 40%;
   }
  `],
})

export class LoginComponent implements OnInit {
  model: any = {};
  jwtHelper = new JwtHelperService();
  constructor(
    public authService: AuthService,
    private alertifyService: AlertifyService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  login(): void {
    this.authService.login(this.model).subscribe(
      (next) => {
        this.alertifyService.success('Logged in successfully');
      },
      (error) => {
        this.alertifyService.error(error.statusText);
      },
      () => {
        const decodedToken = this.jwtHelper.decodeToken(localStorage.getItem('token'));
        if (decodedToken.role === 'Admin') {
          this.router.navigate(['/dashboard']);
        } else {
          this.router.navigate(['/balance']);
        }
      }
    );
  }
}
