/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { environment } from 'src/environments/environment';
import { AuthService } from './auth.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';

describe('Service: Auth', () => {
  let service: AuthService;
  let httpMock: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AuthService],
    });
    httpMock = TestBed.get(HttpTestingController);
    service = TestBed.get(AuthService);
    const adminToken =
      'eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJuYW1laWQiOiI1YTA2OGU5My0zOTU1LTQwNjktOWI1Yy1mZTY3M2VmYTY0MWUiLCJ1bmlxdWVfbmFtZSI6Ik1hbGl0aCIsInJvbGUiOiJBZG1pbiIsIm5iZiI6MTU5NTQ3Njc4MywiZXhwIjoxNTk1NTYzMTgzLCJpYXQiOjE1OTU0NzY3ODN9.xEeYeqvt2dUVWxzaM3HsCwazFoA3P5gdyou39GcfQTjrIOQzyDXapg2GiQn_bV1kV1VtbykZFPQyPUa1K9DPkg';
    localStorage.setItem('token', adminToken);
  });

  afterEach(() => {
    service = null;
    httpMock = null;
  });

  it('login should return Observable with right data', () => {
    const postItem: any = {};
    postItem.username = 'Admin';
    postItem.password = 'admin';
    const baseUrl = environment.apiUrl + 'applicationuser/login';

    service.login(postItem).subscribe((data: any) => {});

    const req = httpMock.expectOne(baseUrl);
    expect(req.request.method).toBe('POST');
    req.flush(postItem);
    httpMock.verify();
  });

  it('register should return Observable with right data', () => {
    // tslint:disable-next-line:prefer-const
    const postItem = {
      username: 'TestAdmin1',
      password: 'testadmin',
      role: 'Admin',
    };

    const baseUrl = environment.apiUrl + 'applicationuser/register';
    service.register(postItem).subscribe((data: any) => {});
    const req = httpMock.expectOne(baseUrl);
    expect(req.request.method).toBe('POST');
    req.flush(postItem);
    httpMock.verify();
  });

  it('roleMatch should return true for correct role match', () => {
    const allowedRoles = ['Admin'];
    expect(service.roleMatch(allowedRoles)).toBeTruthy();
  });

  it('roleMatch should return false for incorrect role match', () => {
    const allowedRoles = ['User'];
    expect(service.roleMatch(allowedRoles)).toBeFalsy();
  });
});
