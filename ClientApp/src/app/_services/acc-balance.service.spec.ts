import { Balance } from './../_models/Balance';
/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AccBalanceService } from './acc-balance.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { environment } from 'src/environments/environment';

describe('Service: AccBalance', () => {
  let service: AccBalanceService;
  let httpMock: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [AccBalanceService, HttpClientTestingModule],
    });
    httpMock = TestBed.get(HttpTestingController);
    service = TestBed.get(AccBalanceService);
  });

  afterEach(() => {
    service = null;
    httpMock = null;
  });

  it('saveFile should put as an Observable', () => {
    // tslint:disable-next-line:prefer-const
    let postItem = {
      year: 2020,
      month: 'January',
      rAndD: 1000,
      canteen: 100,
      ceoCar: 1000,
      marketing: 1000,
      parkingFines: 1000
    };

    const baseUrl = environment.apiUrl + 'accbalance';

    service.saveFile(postItem).subscribe(() => {});

    const req = httpMock.expectOne(baseUrl);
    expect(req.request.method).toBe('PUT');
    req.flush(postItem);
    httpMock.verify();
  });

  it('checkExistdBalance should fetch as an Observable', () => {
    // tslint:disable-next-line:prefer-const

    const mockPeriod = {
      year: 2020,
      month: 1
    };
    const baseUrl =
      environment.apiUrl +
      'accbalance/exist/' +
      mockPeriod.year +
      '/' +
      mockPeriod.month;

    service.checkExistdBalance(mockPeriod).subscribe(() => {});

    const req = httpMock.expectOne(baseUrl);
    expect(req.request.method).toBe('GET');
    req.flush(mockPeriod);
    httpMock.verify();
  });

  it('getFilteredBalance should fetch as an Observable', () => {
    // tslint:disable-next-line:prefer-const

    const mockPeriod = {
      year: 2020,
      month: 1
    };
    const baseUrl =
      environment.apiUrl +
      'accbalance/' +
      mockPeriod.year +
      '/' +
      mockPeriod.month;

    service.getFilteredBalance(mockPeriod).subscribe(() => {});

    const req = httpMock.expectOne(baseUrl);
    expect(req.request.method).toBe('GET');
    req.flush(mockPeriod);
    httpMock.verify();
  });

  it('getYearList should fetch as an Observable', () => {
    // tslint:disable-next-line:prefer-const

    const baseUrl = environment.apiUrl + 'accbalance/yearlist';
    service.getYearList().subscribe(() => {});
    const req = httpMock.expectOne(baseUrl);
    expect(req.request.method).toBe('GET');
    httpMock.verify();
  });

  it('getMonthList should fetch as an Observable', () => {
    // tslint:disable-next-line:prefer-const
    const year = 2020;
    const baseUrl = environment.apiUrl + 'accbalance/monthlist/' + year;
    service.getMonthList(year).subscribe(() => {});
    const req = httpMock.expectOne(baseUrl);
    expect(req.request.method).toBe('GET');
    httpMock.verify();
  });

  it('getReport should fetch as an Observable', () => {
    // tslint:disable-next-line:prefer-const

    const mockPeriod = {
      startYear: 2018,
      endYear: 2019,
      startMonth: 6,
      endMonth: 2,
    };
    const baseUrl =
      environment.apiUrl +
      'accbalance/report?startyear=2018&startmonth=6&endyear=2019&endmonth=2';

    service.getReport(mockPeriod).subscribe(() => {});

    const req = httpMock.expectOne(baseUrl);
    expect(req.request.method).toBe('GET');
    req.flush(mockPeriod);
    httpMock.verify();
  });

  it('mapMonthId should return correct month name list', () => {
    // tslint:disable-next-line:prefer-const
    const mockMonthIdList = [1, 2, 3];
    const expectedMonthNameList = [
      { id: 1, monthName: 'January' },
      { id: 2, monthName: 'February' },
      { id: 3, monthName: 'March' },
    ];

    expect(service.mapMonthId(mockMonthIdList)).toEqual(expectedMonthNameList);
  });
});
