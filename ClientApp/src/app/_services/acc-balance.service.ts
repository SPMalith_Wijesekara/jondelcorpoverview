import { Balance } from './../_models/Balance';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { MonthMap } from '../_models/MonthMap';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AccBalanceService {
  baseUrl = environment.apiUrl;
  months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];

  constructor(private http: HttpClient) {}

  saveFile(balanceSheet: Balance): Observable<any> {
    return this.http.put(`${this.baseUrl}accbalance`, balanceSheet);
  }

  checkExistdBalance(accountPeriod: any): Observable<any> {
    return this.http.get(
      `${this.baseUrl}accbalance/exist/${accountPeriod.year}/${accountPeriod.month}`
    );
  }

  getFilteredBalance(accountPeriod: any): Observable<Balance> {
    return this.http.get<Balance>(
      `${this.baseUrl}accbalance/${accountPeriod.year}/${accountPeriod.month}`
    );
  }

  getYearList(): Observable<any[]> {
    return this.http.get<Array<any>>(`${this.baseUrl}accbalance/yearlist`);
  }

  getMonthList(year): Observable<any[]> {
    return this.http.get<Array<any>>(
      `${this.baseUrl}accbalance/monthlist/${year}`
    );
  }

  getReport(accountPeriod): Observable<Balance[]> {
    let params = new HttpParams();
    params = params.append('startyear', accountPeriod.startYear);
    params = params.append('startmonth', accountPeriod.startMonth);
    params = params.append('endyear', accountPeriod.endYear);
    params = params.append('endmonth', accountPeriod.endMonth);

    return this.http.get<Balance[]>(`${this.baseUrl}accbalance/report`, {
      params,
    });
  }

  mapMonthId(monthNumList): MonthMap[] {
    const monthList = [];
    monthNumList.forEach((monthId) => {
      const monthmap: MonthMap = {
        id: monthId,
        monthName: this.months[monthId - 1],
      };
      monthList.push(monthmap);
    });
    return monthList;
  }
}
