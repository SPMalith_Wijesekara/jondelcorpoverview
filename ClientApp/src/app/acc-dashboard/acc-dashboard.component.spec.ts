/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { AccDashboardComponent } from './acc-dashboard.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AccBalanceService } from '../_services/acc-balance.service';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';

describe('AccDashboardComponent', () => {
  let component: AccDashboardComponent;
  let fixture: ComponentFixture<AccDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AccDashboardComponent],
      imports: [HttpClientTestingModule, RouterTestingModule, FormsModule],
      providers: [AccBalanceService, HttpClientTestingModule, FormsModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
