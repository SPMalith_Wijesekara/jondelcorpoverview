import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { AccBalanceComponent } from './acc-balance/acc-balance.component';
import { AccDashboardComponent } from './acc-dashboard/acc-dashboard.component';
import { Routes } from '@angular/router';
import { AuthGuard } from './_guards/auth.guard';
import { ForbiddenComponent } from './forbidden/forbidden.component';

export const appRoutes: Routes = [
  { path: '', component: LoginComponent },

  {
    path: '',
    runGuardsAndResolvers: 'always',
    canActivate: [AuthGuard],
    children: [
      {
        path: 'register',
        component: RegisterComponent,
        data: { roles: ['Admin'] },
      },
      {
        path: 'upload',
        component: FileUploadComponent,
        data: { roles: ['Admin'] },
      },
      {
        path: 'balance',
        component: AccBalanceComponent,
        data: { roles: ['User'] },
      },
      {
        path: 'dashboard',
        component: AccDashboardComponent,
        data: { roles: ['Admin'] },
      },
    ],
  },
  { path: 'login', component: LoginComponent },
  { path: 'forbidden', component: ForbiddenComponent },

  { path: '**', redirectTo: 'login', pathMatch: 'full' },
];
