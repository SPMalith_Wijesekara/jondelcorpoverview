export interface Balance {
  year: number;
  month: string;
  rAndD: number;
  canteen: number;
  ceoCar: number;
  marketing: number;
  parkingFines: number;
}
