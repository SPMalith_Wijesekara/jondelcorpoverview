export interface MonthMap {
  id: number;
  monthName: string;
}
