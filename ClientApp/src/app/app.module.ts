import { AuthGuard } from './_guards/auth.guard';
import { HasRoleDirective } from './_directives/has-role.directive';
import { appRoutes } from './routes';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ClickOutsideModule } from 'ng-click-outside';
import { FileUploadModule } from 'ng2-file-upload';
import { ChartsModule } from 'ng2-charts';
import { JwtModule } from '@auth0/angular-jwt';

import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { AccBalanceComponent } from './acc-balance/acc-balance.component';
import { AccDashboardComponent } from './acc-dashboard/acc-dashboard.component';

import { AlertifyService } from './_services/alertify.service';
import { HttpClientModule } from '@angular/common/http';
import { AccBalanceService } from './_services/acc-balance.service';
import { AuthService } from './_services/auth.service';
import { ForbiddenComponent } from './forbidden/forbidden.component';

export function tokenGetter() {
  return localStorage.getItem('token');
}

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    LoginComponent,
    RegisterComponent,
    FileUploadComponent,
    AccBalanceComponent,
    AccDashboardComponent,
    HasRoleDirective,
    ForbiddenComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    BrowserAnimationsModule,
    ClickOutsideModule,
    FileUploadModule,
    ChartsModule,
    RouterModule.forRoot(appRoutes),
    BsDropdownModule.forRoot(),
    BsDatepickerModule.forRoot(),
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: ['jondellcorp.azurewebsites.net', 'localhost:5000'],
        disallowedRoutes: [
          'jondellcorp.azurewebsites.net/api/auth',
          'localhost:5000/api/auth',
        ],
      },
    }),
  ],
  providers: [AlertifyService, AccBalanceService, AuthService, AuthGuard],
  bootstrap: [AppComponent],
})
export class AppModule {}
