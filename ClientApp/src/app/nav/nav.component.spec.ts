/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { NavComponent } from './nav.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('NavComponent', () => {
  let component: NavComponent;
  let fixture: ComponentFixture<NavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavComponent ],
      imports : [
        RouterTestingModule,
        FormsModule,
        HttpClientTestingModule
      ]
    })
    .compileComponents();
    const adminToken =
      'eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJuYW1laWQiOiJiMjk2ODMzNy1lMmVhLTRlMTYtYTY4NC05NDNjMTRkMDkzYmMiLCJ1bmlxdWVfbmFtZSI6IlVzZXIiLCJyb2xlIjoiVXNlciIsIm5iZiI6MTU5NTUwMTc3MSwiZXhwIjoxNTk1NTg4MTcxLCJpYXQiOjE1OTU1MDE3NzF9.Gq4O7_9DQOuBgBEpaDAhRUz4J6uVM_1aqgqVl6Ez2Vh8DROUk6fMW8rA68Zh6gq52wqJ1gE4wk_lxy8E5N8vTgg';
    localStorage.setItem('token', adminToken);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return the username of the user currently logged in', () => {
    // tslint:disable-next-line:prefer-const

    expect(component.getUsername()).toEqual('User');
  });
});
